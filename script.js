$(document).ready(function(){
    const colors = {"navy": "#001f3f",
                    "olive": "#3D9970",
                    "blue" : "#0074D9",
                    "teal" : "#39CCCC",
                    "aqua" : "#7FDBFF",
                    "yellow1" : "#FFDC00",
                    "yellow2" : "rgb(207, 207, 79)",
                    "orange" : "#FF851B",
                    "red1" : "#FF4136",
                    "red2" : "rgb(182, 47, 40)",
                    "red3" : "rgb(110, 22, 18)"
                }
    const months = {0:"January", 1:"February", 2:"March", 3:"April", 4:"May",5:"June",
                    6:"July", 7:"August",8:"September",9:"October",10:"November",11:"December"};
    
    Object.freeze(colors);
    Object.freeze(months);

    const localData = data["monthlyVariance"];
    const baseTemp = data["baseTemperature"]

    const w = 1600;
    const h = 900;
    const p = 150;

    const yMin = new Date(1,0,0);
    const yMax = new Date(1,11,30);

    //Create Scale
    const xScale = d3.scaleTime();
        xScale.domain([d3.min(localData, (d) =>{
            return d["year"];
        }), d3.max(localData, (d) => {
            return d["year"];
           })]);
        xScale.range([p, w-p]);

    const yScale = d3.scaleTime();
        yScale.domain([yMin,yMax]);
        yScale.range([p,h-p]);

    //Create SVG
    const svg = d3.select(".graph-container")
            .append("svg")
            .attr("width", w)
            .attr("height", h);

    //Create Axes
    const xAxis = d3.axisBottom(xScale)
        .tickFormat(d3.format(2))
        .ticks(20);

    svg.append("g")
        .attr("transform", "translate(0," + parseInt(h-p) +")")
        .attr("id", "x-axis")
        .call(xAxis);

    const yAxis = d3.axisLeft(yScale)
        .tickFormat(d3.timeFormat("%B"));
    svg.append("g")
        .attr("transform", "translate(" + p + ",0)")
        .attr("id", "y-axis")
        .call(yAxis);

    //Create Rects
    svg.selectAll("rect")
        .data(localData)
        .enter()
        .append("rect")
        .attr("class", "cell")
        .attr("x", (d) => {
            return xScale(d["year"])
        })
        .attr("y", (d) =>{
            return yScale(new Date(1, d["month"]-1, 0));
        })
        .attr("data-year", (d)=> d["year"])
        .attr("data-month", (d) => d["month"] - 1)
        .attr("data-temp", (d) => d["variance"])
        .attr("width", (w-2*p)/(d3.max(localData, (d)=>d["year"])- d3.min(localData, (d)=>d["year"])))
        .attr("height", (h-2*p)/12)
        .style("fill",  (d) =>{
            let temp = baseTemp+d["variance"];
            if(temp < 2.8)
                return colors["navy"];
            else if(temp < 3.9)
                return colors["olive"];
            else if(temp < 5)
                return colors["blue"];
            else if(temp < 6.1)
                return colors["teal"];
            else if(temp < 7.2)
                return colors["aqua"];
            else if(temp < 8.3)
                return colors["yellow1"];
            else if(temp < 9.5)
                return colors["yellow2"];
            else if(temp < 10.6)
                return colors["orange"];
            else if(temp < 11.7)
                return colors["red1"];
            else if(temp < 12.8)
                return colors["red2"];
            else
                return colors["red3"];
        })
     
    //Create Axes Labels
    const xLabel = svg.append("text")
        .attr("id", "x-label")
        .attr("x", w/2)
        .attr("y", h-p/2)
        .text("Years")

    const yLabel = svg.append("text")
        .attr("id", "y-label")
        .attr("x", 100)
        .attr("y", 100)
        .attr("transform", "rotate(-90) translate(-550,-50)")
        .text("Months")

    //Create Legend
    const customLegend = [2.8,3.9,5.0,6.1,7.2,8.3,9.5,10.6,11.7,12.8];
    const rectData = Array(10);
    const legend = svg.append("g")
        .attr("id","legend");
    const colorsArray = Object.values(colors);

    legend.selectAll("rect")
        .data(rectData)
        .enter()
        .append("rect")
        .attr("x", (d,i) => p + 50*i+1)
        .attr("y", h-p/2)
        .attr("width", "50")
        .attr("height", "50")
        .style("fill", (d, i)=>{
            return colorsArray[i];
        })
        .style("stroke", "black")
        .style("stroke-width", "3");

    
    const legendScale = d3.scaleLinear();
    legendScale.domain([0, 15]);
    legendScale.range([p, 650]);
    
    const legendAxis = d3.axisBottom(legendScale)
        .tickFormat(d3.format(2))
        .ticks(1)
        .tickValues([1.5, 3, 4.5, 6, 7.5, 9, 10.5, 12 , 13.5])
        .tickFormat((d,i) => customLegend[i])
        .tickSize([8])
        .tickPadding([1]);
        
    legend.append("g")
        .attr("transform", "translate(0,875)")
        .attr("id", "legend-axis")
        .call(legendAxis);

    //hover Tooltip
    const tooltip = $("#tooltip");
    const date = $("#date");
    const maxTemp = $("#max-temp");
    const minTemp =$("#min-temp");

    $(".cell").hover(function(){
        let temp = parseFloat($(this).attr("data-temp"));
        let cell = $(this);
        
        date.html(cell.attr("data-year") + " - " + months[parseInt(cell.attr("data-month"))])
        maxTemp.html((temp + baseTemp).toFixed(2) + " °C");
        minTemp.html((temp).toFixed(2) + " °C");

        tooltip.css("visibility", "visible");
        tooltip.css("left", cell.position().left +5)
        tooltip.css("top", cell.position().top)
        tooltip.attr("data-year", cell.attr("data-year"));
    }, function(){
        tooltip.css("visibility", "hidden");

    });
});